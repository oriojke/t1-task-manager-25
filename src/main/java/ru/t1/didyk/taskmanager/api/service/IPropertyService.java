package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

}
