package ru.t1.didyk.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.model.IWBS;
import ru.t1.didyk.taskmanager.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private String projectId;

    @NotNull
    private Date created = new Date();

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
