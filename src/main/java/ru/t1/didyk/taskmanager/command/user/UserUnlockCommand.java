package ru.t1.didyk.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Unlock user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-unlock";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
