package ru.t1.didyk.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Update user profile.";
    }

    @NotNull
    @Override
    public String getName() {
        return "update-user-profile";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserID();
        System.out.println("[USER PROFILE UPDATE]");
        System.out.println("FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
