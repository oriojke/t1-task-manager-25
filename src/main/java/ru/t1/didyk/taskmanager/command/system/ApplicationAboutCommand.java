package ru.t1.didyk.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: " + getPropertyService().getAuthorName());
        System.out.println("email: " + getPropertyService().getAuthorEmail());
    }
}
