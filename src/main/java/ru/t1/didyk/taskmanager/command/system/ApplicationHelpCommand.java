package ru.t1.didyk.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.model.ICommand;
import ru.t1.didyk.taskmanager.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show command list.";
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@Nullable final ICommand command : commands) System.out.println(command);
    }
}
